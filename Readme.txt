editregion_starmask.py
contact: Ben Granett ben.granett@brera.inaf.it

This is a code to display and modify region files in DS9 in an
automated way.  The workflow goes like this:

# start up
python editregion_starmask.py

This script will start a ds9 session. The ds9 window should pop up and
it will be controlled by the python script.  At the terminal you will
see a prompt to enter a pointing name:

> enter pointing (q to quit): [W1P033] ? 

Enter a valid VIPERS pointing name and the image and mask will appear
in the ds9 window.  The images will be loaded from a local file or
downloaded in TIF format from http://www.brera.inaf.it/~ben/vipersimages

You now have the opportunity to modify the mask.  After you are done,
at the prompt type y to save.  To discard changes, type q.

repeat.


There is a command line option to load a list of pointings from a file.
> python editregion_starmask.py -l pointinglist.txt 
The file should have one pointing name per line.  # lines are ignored.

> cat pointinglist.txt 
# pointings
W1P033
W1P034
W1P035
W1P036
W1P037



To see all options, look at the help:
python editregion_starmask.py -h
