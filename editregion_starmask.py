import sys,os,inspect
# modify the load path to find modules in local subdirectory
for dpath,dnames,filenames in os.walk("local",followlinks=True):
    dpath = os.path.realpath(os.path.abspath(dpath))
    if  dpath.endswith(".egg") or dpath.endswith('site-packages'):
        if dpath not in sys.path:
            sys.path.insert(1, dpath)
import numpy as N
import time
from PIL import Image
import readline
import urllib
import argparse

# requirements
import pyfits
import ds9


######## set the paths to these files
regionsdir = 'regions'
imagesdir = 'images'
serverroot = 'http://www.brera.inaf.it/~ben'

# this environment variable may be required
#os.environ['XPA_METHOD'] = 'local'
# default is 
#os.environ['XPA_METHOD'] = 'inet'
##################################

pyfitsversion = pyfits.__version__
print "loaded pyfits version",pyfitsversion
if  pyfitsversion<3.1:
    print "**** Warning! you are using pyfits version %g. Tests have been done with v3.1"%pyfitsversion

# check that paths exist
if not os.path.exists(regionsdir):
    print "Deadly error! Can't find",regionsdir
    print "Check that the path is correct"
    sys.exit(1)

########################################


D = None
def startds9():
    """ Connect to DS9 with XPA library """

    print "...starting ds9 ...",
    sys.stdout.flush()

    global D
    D = ds9.ds9("%f"%N.random.uniform())
    D.set('view layout vertical')
    time.sleep(.2)
    D.set('view buttons no')
    time.sleep(.2)
    D.set('view magnifier no')
    D.set('view colorbar no')
    time.sleep(.2)
    D.set('view panner no')
    time.sleep(.2)
    D.set('view info no')

    D.set('height 700')
    D.set('width 700')
    D.set('prefs bgcolor blue')
    print "\r",
    sys.stdout.flush()
    
    return D



def checkpointing(pointing):
    """checks if pointing is valid and if it has already been done."""
    try:
        assert(pointing.startswith("W") and len(pointing)==6)
    except:
        return False    
    return True

def loadimage(path):
    """ """
    if not os.path.exists(path):
        return None

    img = Image.open(path)

    try:
        tags = img.tag
    except AttributeError:
        print >>sys.stderr, "Cannot read header from image %s"%path
        return None

    # try to get the header from the TIFF tags
    header = None
    for k in tags.keys():
        s = tags[k]
        if s.startswith("SIMPLE"):
            header = s
            break
        

    if not header==None:
        header=pyfits.Header.fromstring(header)



    ny,nx = img.size
    data = N.array(img.getdata()).reshape((nx,ny))
    data = N.flipud(data)

    fitspath = os.tempnam("tmp_fits/")+".fits"

    hdu = pyfits.PrimaryHDU(data)
    for key in header.keys():
        if key=='COMMENT': continue
        if key=='HISTORY': continue
        if key.startswith("HIERARCH"): continue
        if key.startswith("CO"): continue        # this is important (strip WCS corrections)
        if len(key)>8: continue
        #print key,header[key]
        hdu.header.update(key, header[key])
    hdulist=pyfits.HDUList([hdu])
    hdulist.writeto(fitspath)

    return fitspath


def getimage(pointing, colour):
    """ """
    if not os.path.exists(imagesdir):
        os.mkdir(imagesdir)
    if not os.path.exists('%s/%s'%(imagesdir,colour)):
        os.mkdir('%s/%s'%(imagesdir,colour))

    localpath = '%s/%s/cfhtls_%s_%s_s.tif'%(imagesdir,colour,colour,pointing)
    if os.path.exists(localpath):
        return localpath

    remotepath = '%s/vipersimages/%s/cfhtls_%s_%s_s.tif'%(serverroot,colour,colour,pointing)
    print "fetching image %s ..."%(remotepath),
    sys.stdout.flush()
    try:
        urllib.urlretrieve(remotepath, localpath)
    except IOError:
        print "could not download",remotepath
    print "done"
    #sys.stdout.flush()

    return localpath


def go(pointing, colourlist=['gri'], cuttag='bright'):
    """ """
    # decide on W1 or W4
    if pointing.startswith('W1'):
        field='W1'
    else:
        field='W4'

    outdir = regionsdir
    regfile = '%s/photo_%s_%s.reg'%(regionsdir, field, cuttag)
    if not os.path.exists(regfile):
        print "> can't find region file",regfile
        return

    # make a backup of the region file
    backup = "%s.backup"%regfile
    if not os.path.exists(backup):
        os.system("cp %s %s"%(regfile,backup))

    frameno = 0
    framelist = {}
    fitspathlist = []
    for colour in colourlist:
        # get the image file
        tiffimage = getimage(pointing, colour)

        if not os.path.exists(tiffimage):
            print "> can't find data at %s, continuing"%tiffimage
            continue

        fitspath = loadimage(tiffimage)
        if fitspath == None: continue
        print "loading into ds9: ",fitspath
        fitspathlist.append(fitspath)

        # DS9 configuration
        D.set('single')            # single frame mode
        D.set('frame %i'%frameno)  # switch to frame i
        D.set('file %s'%fitspath)  # load image
    
        #D.set('zscale')
        D.set('zoom to fit')
        frameno+=1

        print "region file",regfile
        D.set('regions load %s'%regfile)

        D.set('regions selectall')
        #D.set('regions width 2')
        #D.set('regions color green')
        D.set('regions coord fk5')
        D.set('regions format ds9')
        D.set('regions selectnone')

        framelist[frameno] = colour

    D.set('frame 0')   # go back to frame 0
    print "frame list",framelist

    while True:
        print "> Are you done editing the region? [y]es and save, or [q]uit and don't save",
        sys.stdout.flush()
        try:
            input = raw_input("? ")
        except EOFError:
            input = 'q'

        input = input.strip().lower()
        if input=='q' or input=='n':
            print "exiting without saving."
            break
        if input=='y' or input=='s':
            print "saving region to %s"%regfile
            D.set('regions save %s'%regfile)

            break
        print "type y or q"

    # delete frames
    D.set('frame delete all')

    # remove temporary fits file
    for fitspath in fitspathlist:
        os.unlink(fitspath)






if __name__=="__main__":

    parser = argparse.ArgumentParser(description='edit VIPERS regions')
    parser.add_argument('-c',metavar='color',type=type('string'),default='gri',
                        choices=['u','g','r','i','z','gri'],
                        help='valid images are u,g,r,i,z,gri')
    parser.add_argument('-s',metavar='select',type=type('string'),default='bright',
                        choices=['bright','faint','ccd','xsc','starfaint'],
                        help='magnitude selection for stars: bright or faint')
    parser.add_argument('-l',metavar='list', type=type('string'))
    parser.add_argument('-a',metavar='start', type=type('string'))

    args=parser.parse_args()
    colour = args.c
    cuttag = args.s
    pointingfile = args.l
    start = args.a

    colour = [colour]

    print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print "CFHTLS image selection:",colour
    print "Star selection tag:",cuttag
    print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

    pointinglist = None
    if not pointingfile == None:
        pointinglist = []
        for line in file(pointingfile):
            p = line.strip()
            if p == "": continue
            if p.startswith("#"): continue
            pointinglist.append(p)

    if not start == None:
        i = pointinglist.index(start)
        pointinglist = pointinglist[i:]
        
    if not pointinglist == None:
        print "The following pointings will be loaded sequentially"
        print pointinglist
        

    
    startds9()
    pointing = 'W1P033'
    pi = 0
    
    while True:
        if not pointinglist == None:
            if pi >= len(pointinglist):
                print "     you finished the pointing list!"
                pointinglist = None
            else:
                pointing = pointinglist[pi]
            
        print "\n> enter pointing (q to quit): [%s]"%pointing,
        sys.stdout.flush()
        try:
            input = raw_input("? ")
        except EOFError:
            break

        input = input.strip()

        if input in ["q","Q"]: break
        

        nextpointing = input.strip()

        # if no new pointing was given, use the default or last pointing
        if not nextpointing == "":
            pointing = nextpointing
            
        # give up if it is not a valid pointing
        if not checkpointing(pointing):
            continue

        # if we are running from a list, increment
        if not pointinglist == None:
            if pointing == pointinglist[pi]:
                pi += 1

        go (pointing, colour, cuttag)

        
    print "> finito <"

