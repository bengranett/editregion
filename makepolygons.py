import sys,os
import numpy as N

""" convert a general ds9 region file to polygon data structures """


def getcap(rad,np=15):
    """ get a spherical cap. """
    r = rad*N.pi/180
    th = N.arange(0,2*N.pi, 2*N.pi/np)
    z = N.cos(r)
    x = N.sin(r)*N.cos(th)
    y = N.sin(r)*N.sin(th)

    return x,y,z

def rotate(center, xyz):
    """ """
    x,y,z = xyz
    ra,dec = center
    
    # rotate
    cosdec = N.cos(-N.pi/2+dec*N.pi/180)
    sindec = N.sin(-N.pi/2+dec*N.pi/180)
    cosra = N.cos(ra*N.pi/180)
    sinra = N.sin(ra*N.pi/180)

    x2 = cosdec*x - sindec*z
    y2 = y
    z2 = sindec*x + cosdec*z

    x3 = cosra*x2 - sinra*y2
    y3 = sinra*x2 + cosra*y2
    z3 = z2

    # conv to lon lat
    raout = N.arctan2(y3,x3)*180/N.pi
    decout = N.arcsin(z3)*180/N.pi

    raout = raout%360
    
    return raout,decout

def getfloat(s):
    units = 1
    if s.endswith("\""):
        units = 1./3600
        x = float(s[:-1])
    else:
        x = float(s)
    return x*units

def getnums(line):
    a = line.index("(")
    b = line.index(")")
    line = line.replace(","," ")
    return [getfloat(v) for v in line[a+1:b].split()]



def circleToPoly(line, nvert=20):
    """ """
    cra,cdec,radius = getnums(line)
    center = cra,cdec
    
    xyz = getcap(radius, np = nvert)
    ra,dec = rotate(center, xyz)

    points = []
    for i in range(len(ra)):
        points.append(ra[i])
        points.append(dec[i])

    s = ",".join([str(v) for v in points])
    return "polygon(%s)"%s



def boxToPoly(line):
    """ """
    ra,dec,width,height = getnums(line)[:4]

    
    c = N.cos(dec*N.pi/180)

    w = width/2./c
    h = height/2.

    x = ra-w,ra+w,ra+w,ra-w
    y = dec-h,dec-h,dec+h,dec+h
    
    points = []
    for i in range(len(x)):
        points.append(x[i])
        points.append(y[i])
        
    s = ",".join([str(v) for v in points])
    return "polygon(%s)"%s


def go(infile, outfile):
    """ """
    out = file(outfile,'w')
    for line in file(infile):
        line = line.strip()
        
        if line.startswith("circle"):
            s = circleToPoly(line)
        elif line.startswith("box"):
            s = boxToPoly(line)
        else:
            s = line
            
        print >>out, s
    out.close()


if __name__=="__main__":
    for f in sys.argv[1:]:
        r,e = os.path.splitext(f)
        outf = "%s.poly%s"%(r,e)

        print f,outf
        go(f, outf)
