# install script

ROOTDIR=$PWD/local

cd packages

export PYTHONPATH=$PYTHONPATH:$ROOTDIR/lib/python2.7/site-packages/

# install numpy
tar xvf numpy-1.6.2.tar.gz
cd numpy-1.6.2
python setup.py install --prefix=$ROOTDIR
cd ..
rm -rf numpy-1.6.2

# install pyfits
tar xzf pyfits-3.1.tar.gz
cd pyfits-3.1
python setup.py install --prefix=$ROOTDIR
cd ..
rm -rf pyfits-3.1


# install pyds9-1.4.tar.gz
tar xzf pyds9-1.4.tar.gz
cd pyds9-1.4
python setup.py install --prefix=$ROOTDIR
cd ..
rm -rf pyds9-1.4


# done
cd ..
