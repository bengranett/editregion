regionsW1 = regions/photo_W1_bright.poly.reg regions/photo_W1_starmid.poly.reg regions/photo_W1_starfaint.poly.reg regions/photo_W1_xsc.poly.reg regions/photo_W1_ccd.poly.reg 
regionsW4 = regions/photo_W4_bright.poly.reg regions/photo_W4_starmid.poly.reg regions/photo_W4_starfaint.poly.reg regions/photo_W4_xsc.poly.reg regions/photo_W4_ccd.poly.reg 

poly:
	rm -f regions/*.poly.reg
	python makepolygons.py regions/*.reg
concat:
	cat $(regionsW1) > regions/photo_W1.reg
	cat $(regionsW4) > regions/photo_W4.reg
install:
	cp regions/*.reg ~/Dropbox/canvas/regions
	python ~/Dropbox/canvas/region2svg.py ~/Dropbox/canvas/regions/*.reg
